<div align="center">
<!-- <img src="https://rishavanand.github.io/static/images/greetings.gif" align="center" style="width: 50%" /> -->
<img src="https://i.pinimg.com/originals/ce/69/4f/ce694f560636dffcf42ecf40d4f2f962.gif" align="center" style="width: 100%" />
</div>  

### <div align="center">Atualmente estudando IOS 👨‍💻 desenvolvedor desde 2021 🚀</div>  

<div align="center">
<a href="https://linkedin.com/in/lucas-gomes-vieira-81429017a" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>
<a href="https://instagram.com/lucass.viieira" target="_blank">
<img src=https://img.shields.io/badge/instagram-%23000000.svg?&style=for-the-badge&logo=instagram&logoColor=white alt=instagram style="margin-bottom: 5px;" />
</a>
<a href="https://gitlab.com/dev-lucas" target="_blank">
<img src=https://img.shields.io/badge/gitlab-330F63.svg?&style=for-the-badge&logo=gitlab&logoColor=white alt=gitlab style="margin-bottom: 5px;" />
</a>  
<a href="https://github.com/LucasVieiraa" target="_blank">
<img src=https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white alt=gitlab style="margin-bottom: 5px;" />
</a> 
</div>  

<br/>
<br/>
